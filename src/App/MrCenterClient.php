<?php

namespace Gupo\MiddleOfficeMr\App;


use Gupo\MiddleOffice\Clients\Client;
use Gupo\MiddleOffice\Config\Config;
use Gupo\MiddleOffice\Exception\ClientException;
use Gupo\MiddleOffice\VO\RequestHeader;
use GuzzleHttp\Exception\GuzzleException;

class MrCenterClient extends Client
{

    public function __construct()
    {
        parent::__construct(new Config());
    }


    /**
     * 获取机构列表
     * @param array $body
     * @param string $endpoint
     * @param array $headerEx
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function getOrgList(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/org/list');
    }

    /**
     * 获取科室列表
     * @param array $body
     * @param string $endpoint
     * @param array $headerEx
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function getDptList(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/dpt/list');
    }

    /**
     * 获取职工列表
     * @param array $body
     * @param string $endpoint
     * @param array $headerEx
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function getStaffList(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/staff/list');
    }

    /**
     * 获取职工详情接口
     * @param array $body
     * @param string $endpoint
     * @param array $headerEx
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function getStaffDetail(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/staff/detail');
    }

    /**
     * 获取病区列表
     * @param array $body
     * @param string $endpoint
     * @param array $headerEx
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function getWardList(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/ward/list');
    }


    /**
     * 获取排班列表  获取检验检查列表
     * @param array $body
     * @param string $endpoint
     * @param array $headerEx
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function getResourceList(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/resource/list');
    }


    /**
     * 预约检验检查项目
     * @param array $body
     * @param string $endpoint
     * @param array $headerEx
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function getResourceCreate(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/resource/create');
    }


    /**
     * 获取机构下医生团队列表
     * @param array $body
     * @param string $endpoint
     * @param array $headerEx
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function getDoctorTeamList(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/doctor-team/list');
    }

    /**
     * 获取医生团队详情
     * @param array $body
     * @param string $endpoint
     * @param array $headerEx
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function getDoctorTeamDetail(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/doctor-team/detail');
    }

    /**
     * 获取医生团队详情(多团队)
     * @param array $body
     * @param string $endpoint
     * @param array $headerEx
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function getDoctorTeamDetails(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/doctor-team/details');
    }

    /**
     * 海南下沉团队列表
     * @param array $body
     * @param string $endpoint
     * @param array $headerEx
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function getSinkDoctorTeamList(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/doctor-team/sink-list');
    }

    /**
     * 获取通用字典分组列表
     * @param array $body
     * @param string $endpoint
     * @param array $headerEx
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function getDictionaryList(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/dictionary/list');
    }


    public function doctorTeamNewCreate(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/doctor-team-new/create');
    }

    public function doctorTeamNewDelete(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/doctor-team-new/delete');
    }

    public function doctorTeamNewUpdate(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/doctor-team-new/update');
    }

    public function doctorTeamNewDetail(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/doctor-team-new/detail');
    }

    public function doctorTeamNewList(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/doctor-team-new/list');
    }

    public function doctorTeamNewCodeList(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/doctor-team-new/team-code-list');
    }

    public function orgAreaCodeList(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/org/area');
    }

    /**
     * 获取公卫医生团队列表
     * @param array $body
     * @param string $endpoint
     * @param array $headerEx
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function getPublicDoctorTeamList(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/doctor-team-new/public-list');
    }

    /**
     * 获取公卫医疗机构
     * @param array $body
     * @param string $endpoint
     * @param array $headerEx
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function getPublicOrg(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/doctor-team-new/public-org');
    }

    /**
     * 获取地址下级详情
     * @param array $body
     * @param string $endpoint
     * @param array $headerEx
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function getAreas(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/area/get-areas-new');
    }

    /**
     * 获取机构类型列表
     * @param array $body
     * @param string $endpoint
     * @param array $headerEx
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function getOrgTypeList(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/org-type-list');
    }

    /**
     * 排班科室列表
     * @param array $body
     * @param string $endpoint
     * @param array $headerEx
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function getDeptList(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/timeSchedules/get-dept-list');
    }

    /**
     * 排班职工列表
     * @param array $body
     * @param string $endpoint
     * @param array $headerEx
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function getScheduleStaffList(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/timeSchedules/get-schedule-staff-list');
    }

    /**
     * 排班列表
     * @param array $body
     * @param string $endpoint
     * @param array $headerEx
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function getStaffSchedulesList(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/timeSchedules/get-staff-schedules-list');
    }

    public function getOrgHstCodeByVillageCode(array $body, string $endpoint, array $headerEx = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerEx), $body, trim($endpoint, '/') . '/innerapi/org/get-org-hst-code');
    }
}